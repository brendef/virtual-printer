var fs = require('fs')
var Printer = require('ipp-printer')
var im = require('imagemagick')

var printer = new Printer({
    name: 'smartslip-printer',
    port: 3002
})

let filename = ''

printer.on('job', function (job) {

    filename = 'job-' + job.id + '.ps'
    const newPSFilename = './out/' + filename

    var file = fs.createWriteStream(newPSFilename)
    
    job.pipe(file)

    im.convert([newPSFilename, '../slipspng/' + filename.slice(0, -3) + '.png'])

})

